# Terraform for Kubernetes Cluster on AWS


```

├── backend.tf           # State file Location Configuration
├── s3-state.tf          # State file s3 location 
├── eks.tf               # Amazon EKS Configuration
├── gitlab-agent.tf      # Adding kubernetes cluster agent and auth (in order to use helm also in future)
├── group_cluster.tf     # Registering kubernetes cluster to GitLab `apps` Group
└── vpc.tf               # AWS VPC Configuration
```
